'use strict';
const models = require('../models');

const getTasksByUser = function(msg) {
  return models.Task.find()
    .sort('user')
    .then(tasks => {
      const users = [];
      let currentUser;
      tasks.forEach(task => {
        if (!currentUser || currentUser.name !== task.user) {
          if (currentUser) {
            users.push(currentUser);
          }
          currentUser = {
            name: task.user,
            tasks: []
          }
        }
        currentUser.tasks.push(task);
      });
      if (currentUser) {
        users.push(currentUser);
      }
      return users;
    });
}

const saveNew = function(task) {
  const newTask = models.Task(task);
  return newTask.save();
}

const remove = function(num) {
  return models.Task.remove({
    num
  });
}

const changeUser = function(data) {
  return models.Task.findOneAndUpdate({
    num: data.num
  }, {
    user: data.user
  });
}

module.exports = {
  getTasksByUser,
  saveNew,
  remove,
  changeUser
}
