'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AutoIncrement = require('mongoose-sequence')(mongoose);

const taskSchema = new Schema({
  num: Number,
  user: String,
  text: String,
}, {
  timestamps: true
});
taskSchema.plugin(AutoIncrement, {inc_field: 'num'});

module.exports = mongoose.model('Task', taskSchema);
