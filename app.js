'use strict';
const TeleBot = require('telebot');
const providers = require('./providers');
require('dotenv').load();

const mongoose = require('mongoose');
mongoose.connect('mongodb://' + process.env.MONGODB_HOST + ':' + process.env.MONGODB_PORT + '/' + process.env.MONGODB_DB);

const bot = new TeleBot({
  token: process.env.BOT_ID
});

bot.on('/ayuda', msg => {
  let ayuda = '';
  ayuda += '- Agregar asignación\n';
  ayuda += '/nueva {persona} {asignación}\n';
  ayuda += '- Eliminar asignación\n';
  ayuda += '/eliminar {asignación num}\n';
  ayuda += '- Listar todo\n';
  ayuda += '/listar\n';
  ayuda += '- Reasignar a otra persona\n';
  ayuda += '/reasignar {asignación num} {persona}\n';
  return bot.sendMessage(msg.chat.id, ayuda);
});

bot.on('/listar', msg => {
  return providers.Task.getTasksByUser()
    .then(users => {
      let resp = '';
      users.forEach(user => {
        resp += '- ' + user.name + '\n';
        user.tasks.forEach(task => {
          resp += task.num + '. ' + task.text + '\n';
        });
      });
      if (resp == '') {
        resp = 'No tengo nada loco.';
      }
      return bot.sendMessage(msg.chat.id, resp);
    })
    .catch(err => {
      console.error(err);
    });
});

bot.on('/nueva', msg => {
  const parts = msg.text.split(' ');
  parts.shift();
  if (parts.length == 0) {
    return bot.sendMessage(msg.chat.id, 'Mandaste cualquiera. Es:\n/nueva {persona} {asignación}\nSino fijate en /ayuda');
  }
  let user = parts[0];
  user = user.charAt(0).toUpperCase() + user.substr(1).toLowerCase();
  parts.shift();
  if (parts.length == 0) {
    return bot.sendMessage(msg.chat.id, 'Mandaste cualquiera. Es:\n/nueva {persona} {asignación}\nSino fijate en /ayuda');
  }
  const text = parts.join(' ');
  return providers.Task.saveNew({
    user,
    text
  })
    .then(() => {
      return bot.sendMessage(msg.chat.id, 'Guardado!');
    })
    .catch(err => {
      console.error(err);
    });
});

bot.on('/eliminar', msg => {
  const parts = msg.text.split(' ');
  parts.shift();
  if (parts.length == 0) {
    return bot.sendMessage(msg.chat.id, 'Mandaste cualquiera. Es:\n/eliminar {asignación num}\nSino fijate en /ayuda');
  }
  const num = parts[0];
  return providers.Task.remove(num)
    .then(() => {
      return bot.sendMessage(msg.chat.id, 'Borrado!');
    })
    .catch(err => {
      console.error(err);
    });
});

bot.on('/reasignar', msg => {
  const parts = msg.text.split(' ');
  parts.shift();
  if (parts.length == 0) {
    return bot.sendMessage(msg.chat.id, 'Mandaste cualquiera. Es:\n/reasignar {asignación num} {persona}\nSino fijate en /ayuda');
  }
  let num = parts[0];
  parts.shift();
  if (parts.length == 0) {
    return bot.sendMessage(msg.chat.id, 'Mandaste cualquiera. Es:\n/reasignar {asignación num} {persona}\nSino fijate en /ayuda');
  }
  let user = parts[0];
  user = user.charAt(0).toUpperCase() + user.substr(1).toLowerCase();
  return providers.Task.changeUser({
    user,
    num
  })
    .then(() => {
      return bot.sendMessage(msg.chat.id, 'Cambiado!');
    })
    .catch(err => {
      console.error(err);
    });
});

bot.start();
